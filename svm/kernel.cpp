#include "kernel.h"

#include <iostream>
#include <algorithm>

namespace svm
{
    Kernel::Kernel(
                Scheduler scheduler,
                std::vector<Memory::ram_type> executables_paths
            )
        : board(),
          processes(),
          priorities(),
          scheduler(scheduler),
          _last_issued_process_id(0),
          _last_ram_position(0),
          _cycles_passed_after_preemption(0),
          _current_process_index(0)
    {

        // Memory Management

        
        //     Initialize data structures for methods `AllocateMemory` and
        //       `FreeMemory`
        
        _last_free_block_index = 0;
		board.memory.ram[0] = 0;
        board.memory.ram[1] = Memory::DEFAULT_RAM_SIZE - 2;
        
        board.pic.isr_4 = [&]() {
            auto faulting_page_index = board.cpu.registers.a;
            auto free_frame = board.memory.AcquireFrame();
            
            if (free_frame != Memory::INVALID_PAGE) {
                board.memory.page_table->at(faulting_page_index) = free_frame;
            } else {
                std::cout << "No more physical memory." << std::endl;
                board.Stop();
            }
        };

        // Process Management

        std::for_each(
            executables_paths.begin(),
            executables_paths.end(),
            [&](Memory::ram_type &executable) {
                CreateProcess(executable);
            }
        );

        /*
         *  
         *
         *    Switch to the first process on the CPU
         *    Switch the page table in the MMU to the table of the current
         *      process
         *    Set a proper state for the first process
         */

        /*
         *  
         *
         *    Each scheduler should get the page table switching step
         *    Each exit call handler in `isr_3` should start using `FreeMemory`
         *      to release data in RAM or virtual memory
         */
        if (scheduler == FirstComeFirstServed || scheduler == ShortestJob || scheduler == RoundRobin) {
			if (!processes.empty()) {
				_current_process_index = 0;
				board.memory.page_table = processes[_current_process_index ].page_table;
				board.cpu.registers = processes[_current_process_index ].registers;
				processes[_current_process_index ].state = Process::States::Running;
			} else {
                board.Stop();
            }
		} else if (scheduler == Priority) {
			if (!priorities.empty()) {
				Process temp = priorities.top();
				temp.state = Process::States::Running;
				board.memory.page_table = temp.page_table;
				board.cpu.registers = temp.registers;
				priorities.pop();
                priorities.push(temp);
			} else {
                board.Stop();
            }
		}
        
        if (scheduler == FirstComeFirstServed) {
            board.pic.isr_0 = [&]() {

                // Process the timer interrupt for the FCFS
				std::cout << "Timer interrupt for the FCFS was called." << std::endl;
				std::cout << std::endl;
				return;
            };

            board.pic.isr_3 = [&]() {

                // Process the first software interrupt for the FCFS                
				std::cout << "Software interrupt for the FCFS was called." << std::endl << "Number of Processes left = " << processes.size() << std::endl;
                FreeMemory(processes[_current_process_index].memory_start_position);
				// Unload the current process
				std::cout << "Unload the current process with id " << processes[_current_process_index].id << std::endl;
				processes.erase(processes.begin());
				if (processes.empty()) {
					std::cout << "There are no more processes. Stop the board." << std::endl;
					board.Stop();
				} else {
                    _current_process_index = 0;
					processes[_current_process_index].state = Process::States::Running;
				}
				std::cout << std::endl;
            };
        } else if (scheduler == ShortestJob) {
            board.pic.isr_0 = [&]() {

                // Process the timer interrupt for the Shortest Job scheduler
				std::cout << "Timer interrupt for the Shortest Job scheduler was called." << std::endl;
				std::cout << std::endl;
				return;
            };

            board.pic.isr_3 = [&]() {

                // Process the first software interrupt for the Shortest Job scheduler
				std::cout << "Software interrupt for the Shortest Job scheduler was called." << std::endl << "Number of Processes left = " << processes.size() << std::endl;
				FreeMemory(processes[_current_process_index].memory_start_position);
				// Unload the current process
				std::cout << "Unload the current process with id " << processes[_current_process_index].id << std::endl;
				processes.erase(processes.begin());
				if (processes.empty()) {
					std::cout << "There are no more processes. Stop the board." << std::endl;
					board.Stop();
				} else {
                    _current_process_index = 0;
					processes[_current_process_index].state = Process::States::Running;
				}
				std::cout << std::endl;
            };
		
        } else if (scheduler == RoundRobin) {
			board.pic.isr_0 = [&]() {

				// Process the timer interrupt for the Round Robin scheduler
				std::cout << "Timer interrupt for the RoundRobin scheduler was called." << std::endl;
					if (processes.size() < 2) {
						std::cout << "No more processes. Ignore the timer interrupt." << std::endl;
						return;
					}
				++_cycles_passed_after_preemption;
				std::cout << "The current cycle count: " << _cycles_passed_after_preemption << std::endl;
				if (_cycles_passed_after_preemption > _MAX_CYCLES_BEFORE_PREEMPTION) {
					processes[_current_process_index].registers = board.cpu.registers;
					processes[_current_process_index].state = Process::States::Ready;
                    if (_current_process_index < processes.size() - 1) {
                        ++_current_process_index;
                    }
                    else {
                        _current_process_index = 0; 
                    }
                    board.memory.page_table = processes[_current_process_index ].page_table;
					board.cpu.registers = processes[_current_process_index].registers;
					processes[_current_process_index].state = Process::States::Running;
					_cycles_passed_after_preemption = 0;
					
				}
				std::cout << std::endl;
			};

			board.pic.isr_3 = [&]() {

				// Process the first software interrupt for the Round Robin scheduler
				std::cout << "Software interrupt for the RoundRobin scheduler was called." << std::endl << "Number of Processes left = " << processes.size() << std::endl;
				FreeMemory(processes[_current_process_index].memory_start_position);
				// Unload the current process
				std::cout << "Unload the current process with id " << processes[_current_process_index].id << std::endl;
				processes.erase(processes.begin() + _current_process_index);
				if (processes.empty()) {
					board.Stop();
					std::cout << "There are no more processes. Stop the board." << std::endl;
				}
				else {
					if (_current_process_index >= processes.size()) {
						_current_process_index = 0;
					}
					board.cpu.registers = processes[_current_process_index].registers;
                    board.memory.page_table = processes[_current_process_index ].page_table;
					processes[_current_process_index].state = Process::States::Running;
					std::cout << "Status of the process with id " << processes[_current_process_index].id << " has been changed to Running" << std::endl;
					_cycles_passed_after_preemption = 0;
				}
				std::cout << std::endl;
				
			};
		} else if (scheduler == Priority) {
            board.pic.isr_0 = [&]() {

                // Process the timer interrupt for the Priority Queue, using priorities data structure
				std::cout << "Timer interrupt for the Priority scheduler was called." << std::endl;
				if (!priorities.empty()) {
					Process temp = priorities.top();
					
					std::cout << "process id: " << priorities.top().id << "; priority: " << priorities.top().priority << std::endl;
					priorities.pop();
					if (temp.priority > 0) {
						temp.priority -= 1;
						
						priorities.push(temp);
					}	
					if (!priorities.empty()) {
					
						temp = priorities.top();
						temp.registers = board.cpu.registers;
						temp.state = Process::States::Ready;

						priorities.pop();
						priorities.push(temp);
						
						temp = priorities.top();
						board.cpu.registers = temp.registers;
						temp.state = Process::States::Running;
						std::cout << "Process state is changed to Running; process id: " << priorities.top().id << std::endl;
						priorities.pop();
						priorities.push(temp);

						_cycles_passed_after_preemption = 0;
						std::cout << std::endl;
					} else {
						std::cout << "There are no more processes. Stop the board." << std::endl;
						board.Stop(); 
					}				
				}
            };

            board.pic.isr_3 = [&]() {
				
				if (priorities.empty()) {
					board.Stop();
					std::cout << "There are no more processes. Stop the board." << std::endl;
				}
				else {
                    Process temp = priorities.top();
                    FreeMemory(temp.memory_start_position);
                    priorities.pop();
                    if (!priorities.empty()) {

                    }
									
                    temp = priorities.top();
                    FreeMemory(temp.memory_start_position);
					temp.state = Process::States::Running;
                    
                    board.memory.page_table = temp.page_table;
                    board.cpu.registers = temp.registers;
                    priorities.pop();
					priorities.push(temp);
				}
				std::cout << std::endl;
            };
        }

        board.Start();
    }

    Kernel::~Kernel() { }

    void Kernel::CreateProcess(Memory::ram_type &executable)
    {
        Memory::ram_size_type
            new_memory_position = AllocateMemory(executable.size()); // 
                                      //   allocate memory for the process
                                      //   with `AllocateMemory`
        if (new_memory_position == NO_FREE_LARGE_ENOUGH_BLOCK) {
            std::cerr << "Kernel: failed to allocate memory."
                      << std::endl;
        } else {
            // Assume that the executable image size can not be greater than
            //   a page size
            std::copy(
                executable.begin(),
                executable.end(),
                board.memory.ram.begin() + new_memory_position
            );

            Process process(
                _last_issued_process_id++,
                new_memory_position,
                new_memory_position + executable.size()
            );

            if (scheduler == Priority) {
                priorities.push(process);
                std::cout << "process " << process.id << " with priority: " << process.priority << std::endl << std::endl;
            } else {
                processes.push_back(process);
            }
            
            if (scheduler == ShortestJob) {
                Kernel::process_list_type t = processes;
    
                for (int i = 1; i < processes.size(); ++i) {
                    Process k = processes[i];
                    int j = i - 1;
                    while (j > -1 && processes[j].sequential_instruction_count < k.sequential_instruction_count) {
                        processes[j+1] = processes[j];
                        --j;
                    }
                    processes[j+1] = k;
                }
            }
        }
    }

    Memory::ram_size_type Kernel::AllocateMemory(
                                      Memory::ram_size_type units
                                  )
    {
        /*
         *  
         *
         *    Task 1: allocate physical memory by using a free list with the
         *      next fit approach.
         *
         *    You can adapt the algorithm from the book The C Programming
         *      Language (Second Edition) by Brian W. Kernighan and Dennis M.
         *      Ritchie (8.7 Example - A Storage Allocator).
         *
         *    Task 2: adapt the algorithm to work with your virtual memory
         *      subsystem.
         */

         units += 2;


        Memory::ram_size_type current_free_node_index, previous_free_node_index;
        
		auto &ram = board.memory.ram;
		
		
        previous_free_node_index = _last_free_block_index;
        for (current_free_node_index = ram[Translate(previous_free_node_index)]; ;
             previous_free_node_index = current_free_node_index,
             current_free_node_index = ram[Translate(current_free_node_index)]) {
            if (ram[Translate(current_free_node_index + 1)] >= units) {
                if (ram[Translate(current_free_node_index + 1)] == units) {
                    ram[Translate(previous_free_node_index)] =
                        ram[Translate(current_free_node_index)];
                    _last_free_block_index = ram[Translate(current_free_node_index)];
                } else {
                    ram[Translate(current_free_node_index + 1)] -= units;
                    _last_free_block_index = current_free_node_index;
                    current_free_node_index += 
						ram[Translate(current_free_node_index + 1)] + 2;
                    ram[Translate(current_free_node_index + 1)] = units - 2;
                }
				
                return Translate(current_free_node_index + 2);
            }
            if (current_free_node_index == _last_free_block_index) {
                return NO_FREE_LARGE_ENOUGH_BLOCK;
            }
        }

        return NO_FREE_LARGE_ENOUGH_BLOCK;
    }

    void Kernel::FreeMemory(
                     Memory::ram_size_type physical_address
                 )
    {
        /*
         *  
         *
         *    Task 1: free physical memory
         *
         *    You can adapt the algorithm from the book The C Programming
         *      Language (Second Edition) by Brian W. Kernighan and Dennis M.
         *      Ritchie (8.7 Example - A Storage Allocator).
         *
         *    Task 2: adapt the algorithm to work with your virtual memory
         *      subsystem
         */
         Memory::ram_size_type index_of_used_block_header = physical_address - 2;
         Memory::ram_size_type index_of_size_of_used_block = physical_address - 1;
         
         auto &ram = board.memory.ram;
         
         Memory::ram_size_type current_index_of_free_node;
         for (current_index_of_free_node = _last_free_block_index;
                 !(index_of_used_block_header > current_index_of_free_node &&
                 index_of_used_block_header < ram[Translate(current_index_of_free_node)]);
                 current_index_of_free_node = ram[Translate(current_index_of_free_node)]) {
             if (current_index_of_free_node >= ram[Translate(current_index_of_free_node)] &&
                 (index_of_used_block_header > current_index_of_free_node ||
                  index_of_used_block_header < ram[Translate(current_index_of_free_node)])) {
                     break;
                 }
         }
         
         //try to merge with the right block
         if (index_of_used_block_header + ram[Translate(index_of_size_of_used_block)] == 
             ram[Translate(current_index_of_free_node)]) {
             ram[Translate(index_of_size_of_used_block)] += 
                 ram[Translate(ram[Translate(current_index_of_free_node)] + 1)] + 2;
             ram[Translate(index_of_used_block_header)] = 
                 ram[Translate(ram[Translate(current_index_of_free_node)])];
         } else {
             ram[Translate(index_of_used_block_header)] = 
                 ram[Translate(current_index_of_free_node)];
         }
         
         //try to merge with the left block
         if (current_index_of_free_node + 
             ram[Translate(current_index_of_free_node + 1)] + 2 == index_of_used_block_header) {
             ram[Translate(current_index_of_free_node + 1)] += 
                 ram[Translate(index_of_size_of_used_block)] + 2;
             ram[Translate(current_index_of_free_node)] = 
                 ram[Translate(index_of_used_block_header)];
         } else {
             ram[Translate(current_index_of_free_node)] = index_of_used_block_header;
         }
         
         _last_free_block_index = current_index_of_free_node;
    }
    //translate from virtual to physical address
	Memory::ram_size_type Kernel::Translate(Memory::ram_size_type virtual_address) {
        Memory::page_index_offset_pair_type page_index_offset_pair = 
			board.memory.GetPageIndexAndOffsetForVirtualAddress(virtual_address);
		
        Memory::page_entry_type page_frame_index = 
			board.memory.page_table->at(page_index_offset_pair.first);
        Memory::ram_size_type physical_index;
		
        if (page_frame_index == Memory::INVALID_PAGE) {
			auto previous_a = board.cpu.registers.a;
			board.cpu.registers.a = page_index_offset_pair.first;
            
            bool is_there_free_memory = true;
            
            auto faulting_page_index = board.cpu.registers.a;
            auto free_frame = board.memory.AcquireFrame();
            
            if (free_frame != Memory::INVALID_PAGE) {
                board.memory.page_table->at(faulting_page_index) = free_frame;
            } else {
				is_there_free_memory = false;
                std::cout << "No more physical memory" << std::endl;
                board.Stop();
            }

            if (is_there_free_memory) { //if there is a free physical memory calcuate index 
				// Calculate the physical address with the value in the page entry and the
				// physical address offset
				physical_index = page_index_offset_pair.second + 
									Memory::PAGE_SIZE * page_frame_index;
			}
			
			board.cpu.registers.a = previous_a;
        }
         
        return physical_index;
    }
}
