#include "cpu.h"

#include <iostream>

namespace svm
{
    Registers::Registers()
        : a(0), b(0), c(0), flags(0), ip(0), sp(0) { }

    CPU::CPU(Memory &memory, PIC &pic)
        : registers(),
          _memory(memory),
          _pic(pic) { }

    CPU::~CPU() { }

    void CPU::Step()
    {
        int ip =
            registers.ip;

        int instruction =
            _memory.ram[ip];
        int data =
            _memory.ram[ip + 1];

        if (instruction ==
                CPU::MOVA_OPCODE) {
            registers.a = data;
            registers.ip += 2;
        } else if (instruction ==
                       CPU::MOVB_OPCODE) {
            registers.b = data;
            registers.ip += 2;
        } else if (instruction ==
                       CPU::MOVC_OPCODE) {
            registers.c = data;
            registers.ip += 2;
        } else if (instruction ==
                       CPU::JMP_OPCODE) {
            registers.ip += data;
        } else if (instruction ==
                       CPU::INT_OPCODE) {
            switch (data)
            {
                case 1:
                    _pic.isr_3();
                    break;
                    //case 2:
                    //  _pic.isr_5(); // `isr_4` is reserved for page fault
                    //                // exceptions
                    //  break;
                    // ...
            }
            registers.ip += 2;
        
            

            } else if (instruction ==
                            CPU::LDA_OPCODE) {
                // 1. Get a page index and physical address offset from the MMU
                // for a virtual address in 'data'
                Memory::page_index_offset_pair_type page_index = _memory.GetPageIndexAndOffsetForVirtualAddress(data);
                
                //2. Get the page from the current page table in the MMU with
                // the acquired index
                Memory::page_entry_type page = _memory.page_table->at(page_index.first);
                                
                //If the page is invalid
                // 1. Save the value of register 'a' on a stack
                // 2. Place the index of this page into register 'a'
                // 3. Call the page fault handler in PIC (isr_4)
                // 4. Restore register 'a'
                if (page == Memory::INVALID_PAGE) {
                    auto reg_a = registers.a;
                    registers.a = page_index.first;
                    _pic.isr_4();
                    registers.a = reg_a;
                }
                
                // or if the page is valid
                // 1. Calculate the physical address with the value in the
                //     page entry and the physical address offset
                // 2. Read or write (for ST...) from/to the physical memory
                //     with the calculated address
                // 3. Increment the instruction pointer

                else {
                    auto address = page_index.second + Memory::PAGE_SIZE * page;
                    registers.a = _memory.ram[address];
                    ++instruction;
                }

            } else if (instruction ==
                            CPU::LDB_OPCODE) {
                Memory::page_index_offset_pair_type page_index = _memory.GetPageIndexAndOffsetForVirtualAddress(data);
                
                Memory::page_entry_type page = _memory.page_table->at(page_index.first);

                if (page == Memory::INVALID_PAGE) {
                    auto reg_a = registers.a;
                    registers.a = page_index.first;
                    _pic.isr_4();
                    registers.a = reg_a;
                }

                else {
                    auto address = page_index.second + Memory::PAGE_SIZE * page;
                    registers.b = _memory.ram[address];
                    ++instruction;
                }

            } else if (instruction == CPU::LDC_OPCODE) {
                Memory::page_index_offset_pair_type page_index = _memory.GetPageIndexAndOffsetForVirtualAddress(data);
                
                Memory::page_entry_type page = _memory.page_table->at(page_index.first);

                if (page == Memory::INVALID_PAGE) {
                    auto reg_a = registers.a;
                    registers.a = page_index.first;
                    _pic.isr_4();
                    registers.a = reg_a;
                }

                else {
                    auto address = page_index.second + Memory::PAGE_SIZE * page;
                    registers.c = _memory.ram[address];
                    ++instruction;
                }
            } else if (instruction == CPU::STA_OPCODE) {
                Memory::page_index_offset_pair_type page_index = _memory.GetPageIndexAndOffsetForVirtualAddress(data);
                
                Memory::page_entry_type page = _memory.page_table->at(page_index.first);

                if (page == Memory::INVALID_PAGE) {
                    auto reg_a = registers.a;
                    registers.a = page_index.first;
                    _pic.isr_4();
                    registers.a = reg_a;
                }

                else {
                    auto address = page_index.second + Memory::PAGE_SIZE * page;
                    _memory.ram[address] = registers.a;
                    ++instruction;
                }
            } else if (instruction == CPU::STB_OPCODE) {
                Memory::page_index_offset_pair_type page_index = _memory.GetPageIndexAndOffsetForVirtualAddress(data);
                
                Memory::page_entry_type page = _memory.page_table->at(page_index.first);

                if (page == Memory::INVALID_PAGE) {
                    auto reg_a = registers.a;
                    registers.a = page_index.first;
                    _pic.isr_4();
                    registers.a = reg_a;
                }

                else {
                    auto address = page_index.second + Memory::PAGE_SIZE * page;
                    _memory.ram[address] = registers.b;
                    ++instruction;
                }
            } else if (instruction == CPU::STC_OPCODE) {
                Memory::page_index_offset_pair_type page_index = _memory.GetPageIndexAndOffsetForVirtualAddress(data);
                
                Memory::page_entry_type page = _memory.page_table->at(page_index.first);

                if (page == Memory::INVALID_PAGE) {
                    auto reg_a = registers.a;
                    registers.a = page_index.first;
                    _pic.isr_4();
                    registers.a = reg_a;
                }

                else {
                    auto address = page_index.second + Memory::PAGE_SIZE * page;
                    _memory.ram[address] = registers.c;
                    ++instruction;
                }
            
         
         
        } 
        
        else {
            std::cerr << "CPU: invalid opcode data. Skipping..."
                      << std::endl;
            registers.ip += 2;
        }
    }
}
